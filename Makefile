.PHONY: build

# ==================================================================================================
# Variables
# ==================================================================================================

NPM:=$(shell which npm)

# ==================================================================================================
# General
# ==================================================================================================

clean:
	@rm -rf node_modules

npm-install:
	$(NPM) install

dev: clean npm-install

check-env-var:
	@test $(CI_SERVER_URL) || ( echo "CI_SERVER_URL is not set"; exit 1 )
	@test $(CI_PAGES_URL) || ( echo "CI_PAGES_URL is not set"; exit 1 )
	@test $(BOD_CLIENT_ID) || ( echo "BOD_CLIENT_ID is not set"; exit 1 )

serve: check-env-var
	$(NPM) run start

build: check-env-var
	$(NPM) run build

# ==================================================================================================
# Code testing targets
# ==================================================================================================

watch-tests:
	$(NPM) run test
unit-tests:
	$(NPM) run test-ci
ut: unit-tests

# ==================================================================================================
# Code formatting targets
# ==================================================================================================

lint:
	$(NPM) run lint

lint-check:
	$(NPM) run lint-check

style: lint
check: lint-check

sc: style check
sct: style check unit-tests