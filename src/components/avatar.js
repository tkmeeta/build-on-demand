import React, { Component } from "react";
import { isEmpty } from "lodash";
import { userInfo } from "../api";
import "./avatar.css";

export default class Avatar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
    };
  }
  authenticate() {
    const state = encodeURIComponent(
      btoa(
        JSON.stringify({
          path: window.location.pathname + window.location.search,
          created_at: Date.now().toString(),
        })
      )
    );
    localStorage.setItem("state", state);
    const redirectUri =
      encodeURIComponent(process.env.REACT_APP_PAGES_URL) + "/auth.html";
    const clientId = process.env.REACT_APP_CLIENT_ID;
    var url = `${process.env.REACT_APP_GITLAB_URL}/oauth/authorize?client_id=${clientId}&redirect_uri=${redirectUri}&response_type=token&scope=api&state=${state}`;
    window.location = url;
  }
  componentDidMount() {
    let tokenitem = document.cookie.split(";").find((i) => {
      return i.split("=")[0].trim() === "access_token";
    });
    if (tokenitem) {
      window.gitlab_token = tokenitem.split("=")[1];
      userInfo().then((user) => {
        this.setState({ user });
      });
    } else {
      window.gitlab_token = "";
    }

    if (window.gitlab_token === "") {
      this.authenticate();
    }
  }
  render() {
    let avatar = null;
    if (!isEmpty(this.state.user)) {
      avatar = (
        <img
          className="Avatar"
          src={this.state.user.avatar_url}
          alt={this.state.user.name}
        />
      );
    }
    return avatar;
  }
}
